SharedBoard
======

Shared Board is a simple shared board,It was made for fun and for testing twistedmatrix

Graphical frontend with GTK and TwistedMatrix for communications. Auth agaist LDAP.

You must have a LDAP running and the server.

It's just as an example of twistedmatrix.

TODO
-----
* Conf files
* Finish the uncompleted parts
* Do packable
* Different sessions
* ...
* I've no intention to finish it
 

Authors
-------
Francisco José Moreno Llorca - packo@assamita.net - @kotejante

License
-------

	(C) Copyright 2006 Francisco Jose Moreno Llorca <packo@assamita.net>

	Piscos is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Piscos is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Piscos.  If not, see <http://www.gnu.org/licenses/>.
