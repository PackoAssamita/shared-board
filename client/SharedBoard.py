#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Shared Board is a simple shared board,It was made for fun and for testing twistedmatrix

(C) Copyright 2006 Francisco Jose Moreno Llorca <packo@assamita.net>

Shared Board is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Shared Board is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Shared Board.  If not, see <http://www.gnu.org/licenses/>.
"""

"""Shared Board

Basic shared board client.
"""

# modules for GUI
import pygtk
pygtk.require('2.0')
import gobject
import gtk
import gtk.glade
import gtk.gdk
import pango

# modules for Twisted framework
from twisted.internet import gtk2reactor # for gtk-2.0
gtk2reactor.install()
from twisted.internet import reactor
from twisted.internet.protocol import Protocol
from twisted.internet.protocol import ClientFactory
import gc as garbage
import pickle

# globals constants
host = 'localhost'
port = 8011
VERSION = '0.1'
DATA = 'datos/'

class PackoC(Protocol):
    """Packo protocol
        Define the behaviour of the connection protocol.
    """
    
    def dataReceived(self, data):
        #using a list as queue
        self.factory.imagen.append(data)
        self.factory.transporte = self.transport
        
    def connectionLost(self,reason):
        print "Connection lost"

class FabricaProtocolo(ClientFactory):
    """
    Just define the protocol factory
    """
    def __init__(self):
        self.nueva = False
        self.protocol = PackoC
        self.imagen = []
        
# actions
NULA = '0'
RECT = '1'
CIRC = '2'
IMG  = '3'
CHAR = '4'
LINE = '5'
BORR = '6'

# magic numbers
AUTH      = '1368'
WELCOME   = '2461'
DRAW      = '028h'
SEPARADOR = 'd2h9'
DESHACER  = '0zk<'
REHACER   = 'm18s'
ALDIA     = '15vu'
IMAGEN    = 'm238'
IMG_SEP   = '##17'


debug = True


class Pizarra:
    """
        shared board window main class
    """
    actual_action = RECT
    def __init__(self):
    #Start the GTK system with glade
        self.num_mensajes = 0
        self.gui = gtk.glade.XML('pizarra.glade')
        self.gui.signal_autoconnect(self)
        self.setWidgetsFromGladefile()
        self.l_agrupa = [self._tb_rect,self._tb_circ,self._tb_abc]
        self.id_revisa_mens = gobject.timeout_add(200, self.revisa_mens)
        self._aboutdialog1.set_version(VERSION)
        self._principal.set_title('SharedBoard  '+VERSION)
        self._d_pizarra.connect("expose-event", self.on_d_pizarra_expose)
        self.color = gtk.gdk.color_parse('black')
        self.x,self.y = self._d_pizarra.window.get_size()
        # set white
        self.gc = self._d_pizarra.window.new_gc()
        self.gc.set_rgb_fg_color(gtk.gdk.color_parse('white'))
        #gc.set_background(gtk.gdk.color_parse('black'))
        self._d_pizarra.window.draw_rectangle(self.gc,gtk.gdk.TILED,0,0,self.x,self.y)
        self.im_buffer = self._d_pizarra.window.get_image(0,0,self.x,self.y)
        self.im_deshacer = self._d_pizarra.window.get_image(0,0,self.x,self.y)
        self.relleno = 'solido'
        self.insertar_imagen = None
        self.com_buffer = self._t_comentario.get_buffer()
        self.comentario=""
        self.rellenos = {'linea':gtk.gdk.SOLID,'solido':gtk.gdk.TILED}
        self.movido = False
        self.historico = []
        self.dibujados = 0
        # show if the window is hidden
        self.ocultado = False
        self.escrito_ocultado = False
        #del gc
        #garbage.collect()
        
    def revisa_mens(self, *args):
        while len(self.factoria.imagen) > 0:
            actual = self.factoria.imagen.pop()
            if actual[:4] == AUTH:
                if actual[4:] == 'ACCEPT':
                    self._v_autentica.hide()
                    self._principal.set_sensitive(True)
                elif actual[4:] == WELCOME:
                    pass
                else:
                    self._e_user.set_text("")
                    self._e_pass.set_text("")
            elif actual[:4] == DRAW:
                self.remoto = True
                #print "Recibe: "+actual
                par = actual[4:].split(SEPARADOR)
                if par[0] == BORR:
                    del self.historico[int(par[10])]
                    self.dibujados = 0
                    self.gc.set_rgb_fg_color(gtk.gdk.color_parse('white'))
                    self._d_pizarra.window.draw_rectangle(self.gc,gtk.gdk.TILED,0,0,self.x,self.y)
                elif par[0]== IMG:
                    nombre = par[10]
                    try:
                        fic = open(DATA+nombre)
                        fic.close()
                    except:
                        self.recuperar_imagen(nombre)
                    imagen_pixbuf = gtk.gdk.pixbuf_new_from_file(DATA+nombre)
                    # ancho = imagen_pixbuf.get_width()
                    # alto = imagen_pixbuf.get_height()
                    del(imagen_pixbuf)
                else:
                    self.historico.append(par)
                self.dibujar_historico()
                self.remoto = False
            elif actual[:4] == DESHACER:
                if debug == True:
                    print "Undo"
                self.remoto = True
                if self.dibujados == len(self.historico):
                    self.dibujados = 0
                self.historico.pop()
                self.dibujar_historico()
                self.remoto = False
            elif actual[:4] == ALDIA:
                self.remoto = True
                del self.historico
                self.dibujados = 0
                self.historico = []
                for par in pickle.loads(actual[4:]):
                    self.historico.append(par[4:].split(SEPARADOR))
                self.dibujar_historico()
                self.remoto = False
            elif actual[:4] == IMAGEN:
                if debug == True:
                    print "Receiving images"
                nombre,ser = actual[4:].split(IMG_SEP)
                archivo = open(DATA+nombre,'w+')
                archivo.write(pickle.loads(ser))
                archivo.close()
            return True
        return True
        
    def setWidgetsFromGladefile(self):
        """take the widgets from the glade to the class
        """
        widgets = ("e_user","e_pass","b_autentica","v_autentica","elejir_exportar","s_barra","b_letra","t_comentario","b_comentario","comentario","b_archivo","preview","carga_imagen","colorbutton1","tb_circ","d_pizarra","aboutdialog1","principal","tb_rect","tb_circ","b_img","tb_abc")
        gw = self.gui.get_widget
        for widgetName in widgets:
            setattr(self, "_" + widgetName, gw(widgetName))
            
    def accion_seleccionada(self):
        if self.actual_action == RECT:
            fig = "rectangulo\t"
        elif self.actual_action == CIRC:
            fig = "circulo     \t"
        elif self.actual_action == IMG:
            fig = "imagen      \t"
        elif self.actual_action == CHAR:
            fig = "texto      \t"
        elif self.actual_action == BORR:
            fig = "borrar      \t"
        if self.relleno == 'linea':
            rell = " marco\t "
        elif self.relleno == 'solido':
            rell = " solido\t "
        self._s_barra.push(1,"".join(['Mode:\t',fig,rell]))
            
    def on_principal_delete_event(self,*args):
        """stop the twisted machine and exit of the application (gtk2reactor)
        """
        reactor.stop()

    def on_exportar_activate(self,*args):
        self.image = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB,True,8,self.x,self.y)
        self.image.get_from_image(src = self.im_buffer, cmap=self._d_pizarra.window.get_colormap(), src_x=0, src_y=0, dest_x=0, dest_y=0, width=self.x, height=self.y)
        self._elejir_exportar.show()
        
    def on_elejir_exportar_delete_event(self,*args):
        self._elejir_exportar.hide()
        return True
        
    def on_elejir_exportar_file_activated(self,*args):
        archivo = self._elejir_exportar.get_filename()
        self.image.save(".".join([archivo,'png']),'png')       
        self._elejir_exportar.hide()
        del self.image
        garbage.collect()
        
    def on_acerca_de1_activate(self,*args):
        self._aboutdialog1.show()
        
    def on_aboutdialog1_delete_event(self,*args):
        self._aboutdialog1.hide()
        return True
    
    def run(self):
        """start the twisted machine
        """
        self.usuario = ""
        self.factoria = FabricaProtocolo()
        reactor.connectTCP(host,port,self.factoria)
        reactor.run()
        
    def on_tb_rect_clicked(self,*args):
        self.actual_action = RECT
        self.accion_seleccionada()
        
    def on_tb_circ_clicked(self,*args):
        self.actual_action = CIRC
        self.accion_seleccionada()
        
    def on_tb_abc_clicked(self,*args):
        self.actual_action = CHAR
        self.accion_seleccionada()
        self._comentario.show()
        
    def on_b_comentario_clicked(self,*args):
        self.comentario = self.com_buffer.get_text(self.com_buffer.get_start_iter(),self.com_buffer.get_end_iter())
        self._comentario.hide()
        
    def on_comentario_delete_event(self,*args):
        self._comentario.hide()
        return True

    def on_d_pizarra_button_press_event(self,*args):
        self.xa,self.xb = self._d_pizarra.get_pointer()
        
        
    def on_d_pizarra_button_release_event(self,*args):
        a,b = self._d_pizarra.get_pointer()
        letra = pango.FontDescription(self._b_letra.get_font_name())
        if self.actual_action == IMG:
            try:
                if debug == True:
                    print "Inserting image"
                arch = open(self.insertar_imagen,'r')
                leido = arch.read()
                self.mandar_pizarra(self.actual_action,self.relleno,self.color,a,b,letra,pickle.dumps(leido))
                arch.close()
            except Exception,e:
                print "ERROR sending image: %s" % (e)
        elif self.actual_action == BORR:
            borrar = obtener_objeto_borrado(self.historico,self.xa,self.xb)
            self.mandar_pizarra(self.actual_action,self.relleno,self.color,a,b,letra,str(borrar))
        else:
            self.mandar_pizarra(self.actual_action,self.relleno,self.color,a,b,letra,self.comentario)
        
    def on_d_pizarra_motion_notify_event(self,*args):
        self.movido = True
        
    def on_d_pizarra_expose(self,*args):
        """
            two ways, gc is saved and redrawn or every object is drawn
        """
        if debug == True:
            print self.escrito_ocultado
        if self.escrito_ocultado == False :
            if debug == True:
                print "It's exposes, gc is saved"
            # saving gc
            #gc = self._d_pizarra.window.new_gc()
            x,y = self._d_pizarra.window.get_size()
            try:
                self._d_pizarra.window.draw_image(self.gc,self.im_buffer,0,0,0,0,x,y)
                
            except:
                pass
            #del gc
            garbage.collect()
        else:
            if debug == True:
                print "it's exposes and every object is drawn"
            self.remoto = True
            self.dibujados = 0
            self.dibujar_historico()
            self.escrito_ocultado = False
        self.ocultado = False
          
    def on_d_pizarra_visibility_notify_event(self,*args):
        self.ocultado = True
        if debug == True:
            print "hidden..."
        
    def on_b_refresca_clicked(self,*args):
        if debug == True:
            print "Updating"
        if len(self.historico) > 0:
            x,y = self._d_pizarra.window.get_size()
            #gc = self._d_pizarra.window.new_gc()
            self.gc.set_rgb_fg_color(gtk.gdk.color_parse('white'))
            self._d_pizarra.window.draw_rectangle(self.gc,gtk.gdk.TILED,0,0,x,y)
            self.remoto = True
            self.dibujados = 0
            self.dibujar_historico()
        
    def on_b_autentica_clicked(self,*args):
        #self.factoria.transporte.write(AUTH+self._e_user.get_text()+";"+self._e_pass.get_text())
        self._v_autentica.hide()
        self._principal.set_sensitive(True)
        self.factoria.transporte.write(ALDIA)
        
    def on_colorbutton1_color_set(self,*args):
        self.color = self._colorbutton1.get_color()
        
    def on_b_linea_clicked(self,*args):
        self.relleno = 'linea'
        self.accion_seleccionada()
        
    def on_b_solido_clicked(self,*args):
        self.relleno = 'solido'
        self.accion_seleccionada()
        
    def on_b_img_clicked(self,*args):
        self._carga_imagen.show()
        
        
    def on_carga_imagen_delete_event(self,*args):
        self._carga_imagen.hide()
        return True
    
    def on_b_deshacer_clicked(self,*args):
        if len(self.historico) > 0:
            self.gc.set_rgb_fg_color(gtk.gdk.color_parse('white'))
            self._d_pizarra.window.draw_rectangle(self.gc,gtk.gdk.TILED,0,0,self.x,self.y)
            self.factoria.transporte.write(DESHACER)
    def on_b_rehacer_clicked(self,*args):
        self.factoria.transporte.write(REHACER)
        
    def on_b_archivo_file_activated(self,*args):
        archivo = self._b_archivo.get_filename()
        try:
            self._preview.set_from_file(archivo)
            self.insertar_imagen = archivo
        except:
            if debug == True:
                print "No preview available"
        self._carga_imagen.hide()
        if self.insertar_imagen != None:
            self.actual_action = IMG
            self.accion_seleccionada()
            self.actual_action = IMG

    def on_principal_size_request(self, *args):
        if debug == True:
            print "Resizing"

        self.x,self.y = self._d_pizarra.window.get_size()
##        self.gc.set_rgb_fg_color(gtk.gdk.color_parse('white'))
##        if x > self.x:
##            self._d_pizarra.window.draw_rectangle(self.gc,gtk.gdk.TILED,self.x,0,x,y)
##        if y > self.y:
##            self._d_pizarra.window.draw_rectangle(self.gc,gtk.gdk.TILED,0,self.y,x,y)
##        self.x = x
##        self.y = y
    def on_b_borrar_clicked(self,nombre):
        self.actual_action = BORR
        self.accion_seleccionada()
    
    def recuperar_imagen(self,nombre):
        self.factoria.transporte.write(IMAGEN+nombre)
        if debug == True:
            print "Image recovered "+nombre
         
    def dibujar_pizarra(self,accion,relleno,color,xa,xb,a,b,fuente,comentario =""):
        #x,y = self._d_pizarra.window.get_size()
        #print accion,relleno,color,xa,xb,a,b,fuente,comentario
        #escribir texto
        if accion == CHAR:
            #gc = self._d_pizarra.window.new_gc()
            contex = self._d_pizarra.create_pango_context()
            attrList = pango.AttrList()
            atrribute = pango.AttrFontDesc(fuente,0,-1)
            attrList.insert(atrribute)
            com_layout = pango.Layout(contex)
            com_layout.set_text(comentario)
            com_layout.set_attributes(attrList)
            self.im_deshacer = self._d_pizarra.window.get_image(0,0,self.x,self.y)
            self._d_pizarra.window.draw_layout(self.gc,a,b,com_layout,color)
        elif accion == IMG:
            nombre = comentario
            self.im_deshacer = self._d_pizarra.window.get_image(0,0,self.x,self.y)            
            
            try:
                imagen_pixbuf = gtk.gdk.pixbuf_new_from_file(DATA+nombre)
                # ancho = imagen_pixbuf.get_width()
                # alto = imagen_pixbuf.get_height()
                self._d_pizarra.window.draw_pixbuf(self.gc,imagen_pixbuf,0,0,a,b)
                if debug == True:
                    print "image drawn: " + DATA+nombre
            except:
                print "ERROR drawing remote image"
            self.insertar_imagen = None
        # draw shapes
        #elif self.movido == True or self.remoto== True:           
        elif self.remoto== True:           
            # style = self._d_pizarra.get_style()
            # gc = self._d_pizarra.window.new_gc()
            self.gc.set_rgb_fg_color(color)
            self.im_deshacer = self._d_pizarra.window.get_image(0,0,self.x,self.y)
            dibuja(self._d_pizarra,self.gc,self.rellenos[relleno],xa,xb,a,b,accion)
    
    def mandar_pizarra(self,accion,relleno,color,a,b,fuente,comentario =""):
        rojo,verde,azul = serializa_color(color)
        try:
            self.factoria.transporte.write(DRAW+SEPARADOR.join([accion,relleno,rojo,verde,azul,str(self.xa),str(self.xb),str(a),str(b),fuente.to_string(),comentario]))
        except:
            print "ERROR sending board"
        
    def dibujar_historico(self):
        #x,y = self._d_pizarra.window.get_size()
        if debug == True:
            print "Drawing history: "+ str(len(self.historico))        
        for par in self.historico[self.dibujados:]:
            self.dibujar_pizarra(par[0],par[1],deserializa_color(par[2],par[3],par[4]),int(par[5]),int(par[6]),int(par[7]),int(par[8]),pango.FontDescription(par[9]),par[10])
            self.dibujados = self.dibujados+1
            if self.ocultado == True:
                if debug == True:
                    print "It's hidden"
                self.escrito_ocultado = True
            else:
                self.im_buffer = self._d_pizarra.window.get_image(0,0,self.x,self.y)

def obtener_objeto_borrado(lista,xa,xb):
    """
        @return the last object that fill that point
    """
    #par[0],par[1],deserializa_color(par[2],par[3],par[4]),int(par[5]),int(par[6]),int(par[7]),int(par[8]),pango.FontDescription(par[9]),par[10])
    #accion,relleno,rojo,verde,azul,str(self.xa),str(self.xb),str(a),str(b),fuente.to_string(),comentario]))
    contador = 0
    salida = 0
    if debug == True:
        print "Borrar puntero: "+ str(xa)+" "+str(xb)
    for par in range(len(lista)*-1,0):
        
        x,y,xx,yy = normaliza_coordenadas(int(lista[par][5]),int(lista[par][6]),int(lista[par][7]),int(lista[par][8]))
        print x,y,xx,yy
        if xa >= x and xa <= xx:
            if xb >= y and xb <= yy:
                salida = contador
        contador = contador +1
    return salida
    

def dibuja(drawable,gc,fill,x,y,xx,yy,accion):
    x,y,xx,yy = normaliza_coordenadas(x,y,xx,yy)
    if accion == RECT:
        drawable.window.draw_rectangle(gc,fill,x,y,xx-x,yy-y)
    elif accion == CIRC:
        drawable.window.draw_arc(gc,fill,x,y,xx-x,yy-y,0,360*64)
        
def normaliza_coordenadas(x,y,xx,yy):
    if x > xx:
        a = x
        x = xx
        xx = a
    if y > yy:
        a = y
        y = yy
        yy = a
    return x,y,xx,yy

def serializa_color(color):
    #se toma un gtk.gdk.colo
    return str(color.red),str(color.green),str(color.blue)
def deserializa_color(rojo,verde,azul):
    return gtk.gdk.Color(int(rojo),int(verde),int(azul))

if __name__ == '__main__':
    a = Pizarra()
    a.run()