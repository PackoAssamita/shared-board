#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Shared Board is a simple shared board,It was made for fun and for testing twistedmatrix

(C) Copyright 2006 Francisco Jose Moreno Llorca <packo@assamita.net>

Shared Board is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Shared Board is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Shared Board.  If not, see <http://www.gnu.org/licenses/>.
"""

"""Shared Board

Basic shared board server.
"""

#serialize objects
import pickle
from pickle import UnpicklingError
import os
#modules for twisted
from twisted.internet.protocol import Factory
from twisted.internet.protocol import Protocol
from twisted.internet import reactor

#global constants
ARCHIVO_LOG = 'sb_server.log' # path to log file
CREDENTIALS = 'cred.ini' # path to cred.ini
DATA      = 'data_s/'    # data folder

#logging engine
from twisted.python import log
log.startLogging(open(ARCHIVO_LOG,'a'))

#ldap authentication
import ldap
servidor_auth = 'ldapserver:389'

def cadenaLdap(usuario):
    """Return the identification string in LDAP from the value
    in the field sAMAccountName
    
    """
    try:
        l = ldap.open(servidor_auth)
        l.protocol_version = ldap.VERSION3	
    except ldap.LDAPError, e:
        log.msg(e)
    #first login and then search
    try:
        arch = open(CREDENTIALS,'r')
        leido = arch.read()
        lineas = leido.split('\n')
        arch.close()
        print lineas
    except:
        log.msg("error opening credentials file")
    l.simple_bind_s(lineas[0],lineas[1])
    baseDN = lineas[2]
    searchScope = ldap.SCOPE_SUBTREE
    retrieveAttributes = None
    try:
        searchFilter = "cn=%s*%s*" % (usuario.split('.')[0],usuario.split('.')[1])
        ldap_result_id = l.search(baseDN, searchScope, searchFilter, retrieveAttributes)
        while 1:
            result_type, result_data = l.result(ldap_result_id, 0)
            if (result_data == []):
                break
            else:
                if result_type == ldap.RES_SEARCH_ENTRY:
                    log.msg(result_data[0][0])
                    return result_data[0][0]
    except:
        log.msg('Error authenticating against LDAP')

def autentico(usuario, password):
    """
    this function return True if the password identify to the user
    False in other cases
    """
    log.msg("Auth in progress......")
    try:
        log.msg("-------------------------- "+usuario)
        l = ldap.open(servidor_auth)
        l.simple_bind_s(cadenaLdap(usuario),password)
        log.msg("%s Authentication ok" % (usuario))
        return True
    except:
        log.msg('Error authenticating against LDAP')
        return False
    
# actions:
NULA = '0'
RECT = '1'
CIRC = '2'
IMG  = '3'
CHAR = '4'
LINE = '5'
BORR = '6'

# magic numbers
AUTH      = '1368'
WELCOME   = '2461'
DRAW      = '028h'
SEPARADOR = 'd2h9'
DESHACER  = '0zk<'
REHACER   = 'm18s'
ALDIA     = '15vu'
IMAGEN    = 'm238'
IMG_SEP   = '##17'

class Packo(Protocol):
    """Packo protocol
        Define the behaviour of the connection protocol.
    """
    usuario = ""
    password = ""
    
    def dataReceived(self, data):
        """manage the data entry
        """
        if data[:4] == AUTH:
            self.usuario = data[4:].split(';')[0]
            self.password = data[4:].split(';')[1]
            if autentico(self.usuario,self.password) == False:
                self.transport.write(AUTH+"DENIED")
            else:
                self.transport.write(AUTH+"ACCEPT")
        elif data[:4] == DRAW:
            par = data[4:].split(SEPARADOR)
            if par[0] == IMG:
                try:
                    nom = str(len(os.listdir(DATA))+1)
                    deser = pickle.loads(par[10])
                    archivo = open(DATA+nom,'w+')
                    archivo.write(deser)
                    archivo.close()
                    log.msg("Image saved")
                    par[10] = nom
                    comando = SEPARADOR.join(par)
                    comando = DRAW+comando
                    self.factory.historico.append(comando)
                    for c in self.factory.clients:
                        c.mensaje(comando)
                except UnpicklingError,e:
                    log.msg('Error saving image: %s' %(e))
                del self.factory.deshecho
                self.factory.deshecho = []
            elif par[0]== BORR:
                log.msg(par[10])
                self.factory.deshecho.append(self.factory.historico[int(par[10])])
                del self.factory.historico[int(par[10])]
                for c in self.factory.clients:
                        c.mensaje(data)
            else:
                self.factory.datos = data
                self.factory.historico.append(data)
                for c in self.factory.clients:
                    c.mensaje(data)
                del self.factory.deshecho
                self.factory.deshecho = []
        elif data[:4] == DESHACER:
            if len(self.factory.historico) > 0:
                self.factory.deshecho.append(self.factory.historico.pop())
                for c in self.factory.clients:
                    c.mensaje(data)
        elif data[:4] == REHACER:
            if len(self.factory.deshecho) > 0:
                mandar = self.factory.deshecho.pop()
                self.factory.historico.append(mandar)
                for c in self.factory.clients:
                    c.mensaje(mandar)
        elif data[:4] == ALDIA:
            historia = pickle.dumps(self.factory.historico)
            self.mensaje(ALDIA+historia)
        elif data[:4] == IMAGEN:
            archivo = open(DATA+data[4:],'r')
            im = pickle.dumps(archivo.read())
            self.mensaje(IMAGEN+data[4:]+IMG_SEP+im)
            archivo.close()
            print "Image served: "+data[4:]
        
    def connectionMade(self):
        """Add to a list the clients connections.
        """
        self.factory.clients.append(self)
        self.transport.write(WELCOME)
        
    def connectionLost(self,reason):
        """remove the connection from the list
        and send a message to other clients
        """
        
        self.factory.clients.remove(self)
        
    def mensaje(self, datos):
        """send a message
        """
        self.transport.write(datos)

factory = Factory()
factory.protocol = Packo
factory.clients = []
factory.historico = []
factory.deshecho  = []

reactor.listenTCP(8011, factory)
reactor.run()